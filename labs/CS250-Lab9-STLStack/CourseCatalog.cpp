#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <stack>
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

/**
    @param      const string&       code        The course code (e.g., "CS250") to search for.
    @return                         Course      The course whose 'code' variable matches the parameter.

    Use a for-loop to search through all the courses in the
    vector<Course> m_courses member structure. If the current course's code is
    the same as the one passed in as a parameter, return this course.

    OTHERWISE, if you've searched all courses and the function hasn't returned yet,
    that means the course isn't found - throw the CourseNotFound exception.
*/
Course CourseCatalog::FindCourse( const string& code )
{
    string error = "Unable to find course " + code + "!";
    throw CourseNotFound( error );
}

/**
    Use a for-loop to display all courses currently stored, including the
    index, their code, title, and prereqs
*/
void CourseCatalog::ViewCourses()
{
    Menu::Header( "VIEW COURSES" );

    cout << left << setw(5) << "#" << setw(10) << left << "CODE" << setw(50) << "TITLE" << setw(15) << "PREREQS" << endl;
    Menu::DrawHorizontalBar( 80 );

    Menu::Pause();
}

/**
    This menu will ask the user to enter a class code. From that,
    you will try to search for the class we want prereqs for.
    Use a try/catch to find this course. (If there's an exception,
    display an error message and return).

    Next, create a stack of Course objects, called prereqs.
    First, you will push the current course to the list.

    Then, while the current course has a prereq,
    find the prereq of the current course, and store it.
    Push this prereq to the stack.

    Use a try/catch within your while loop to detect exceptions.
    If a course is not found, just break out of the while loop.

    Finally, use a second while loop to go through all the items in the stack,
    displaying the top-most item (first prerequisite) and popping the item
    until the stack of prereqs is empty.
*/
void CourseCatalog::ViewPrereqs()
{
    Menu::Header( "GET PREREQS" );

    string courseCode = Menu::GetStringChoice( "Enter class code" );

    Course current;

    // Try/Catch: Use the FindCourse function to find the course with the code entered.
    // If an exception occurs, display exception.what() and return from the function.


    // Create a stack of Courses called prereqs. Push the current course onto it first.


    // While the current course's prereq is not an empty string ""...
    //  Use a try/catch, and set the current variable to this prereq course.
    //  (Use the FindCourse function). Then, push this new course onto the prereqs stack.
    //  If there's an exception, display the error and break.


    // Use a while loop to keep looping while the prereqs stack is NOT empty...
    //  Display the top-most course's code and naem
    //  Pop the item off the top of the list

}

/**
    Course program main menu
*/
void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}


/**
    This loads the courses from the courses.txt file.
*/
void CourseCatalog::LoadCourses()
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.push_back( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.size() << " courses loaded" << endl << endl;
}
