#include <iostream>
#include <fstream>
#include <list>
#include <string>
using namespace std;

#include "Menu.hpp"

list<string> LoadBook( const string& filename );
void ReadBook( list<string> bookText );

int main()
{
    vector<string> books = { "aesop.txt", "fairytales.txt" };

    bool done = false;
    while ( !done )
    {
        Menu::Header( "LIBRARY" );

        cout << "Which book do you want to read?" << endl;
        int choice = Menu::ShowIntMenuWithPrompt( books );

        list<string> bookText = LoadBook( books[choice-1] );
        ReadBook( bookText );
    }

    return 0;
}

/**
    Open the given file, read in each line into the bookText string,
    and return the bookText string when done.

    @param  const string& filename      The path & filename of item to open.
    @return list<string>                A list of all the lines in the book.
*/
list<string> LoadBook( const string& filename )
{
    list<string> bookText;

    return bookText;
}


/**
    Create a loop that will continue running until the reader decides
    to exit. Draw out an appropriate amount of lines of the book to
    fill the terminal (standard terminal height is 24 lines, assume this is 1 page).

    If the user enters a command to go "back", then iterate backwards two pages.
    If the user enters a command to go "forward", then continue displaying the next set of lines.
    Make sure to include a "quit" command as well.

    @param list<string> bookText       The text of the book to read thru
*/
void ReadBook( list<string> bookText )
{
    Menu::Header( "Reading book" );
}
