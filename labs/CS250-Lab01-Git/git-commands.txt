-------------------------------------------------------------
- SETUP: SET NAME & EMAIL                                   -
- You only have to do this ONCE per machine.                -
-------------------------------------------------------------

    git config --global user.name "YOUR NAME"
    git config --global user.email "YOUR EMAIL"


-------------------------------------------------------------
- SETUP: CLONE A REPOSITORY                                 -
- This pulls the files down from the server.                -
- Only need to do this once on a machine, then use the      -
- commands below to make updates.                           -
-                                                           -
- Get the URL from BitBucket                                -
-------------------------------------------------------------

    git clone URL


-------------------------------------------------------------
- ADD FILES TO A SNAPSHOT                                   -
- Add which files you've changed since last time.           -
-------------------------------------------------------------

    ADD ONE SPECIFIC FILE:
    git add FILENAME

    ADD ALL CPP AND HPP FILES:
    git add *.cpp *.hpp

    ADD ALL UPDATED FILES (WILL ADD JUNK):
    git add .


-------------------------------------------------------------
- MAKE A SNAPSHOT                                           -
- Commit your changes                                       -
-------------------------------------------------------------

    git commit -m "Message of what you did."


-------------------------------------------------------------
- PULL CHANGES FROM SERVER                                  -
- Do this if you're working  on multiple machines or        -
- with another person.                                      -
-------------------------------------------------------------

    git pull



-------------------------------------------------------------
- PUSH CHANGES TO SERVER                                    -
- Save all your changes to the server.                      -
-------------------------------------------------------------

    git push
