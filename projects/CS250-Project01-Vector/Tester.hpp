#ifndef _TESTER_HPP
#define _TESTER_HPP

#include <iostream>
#include <string>
using namespace std;

#include "cuTEST/TesterBase.hpp"
#include "cuTEST/Menu.hpp"
#include "cuTEST/StringUtil.hpp"

#include "DataStructure/Vector.hpp"

class Tester : public TesterBase
{
public:
	Tester()
	{
		AddTest(TestListItem("VectorConstructor()", bind(&Tester::VectorConstructor, this)));
		AddTest(TestListItem("IsEmpty()",           bind(&Tester::IsEmpty, this)));
		AddTest(TestListItem("IsInvalidIndex()",    bind(&Tester::IsInvalidIndex, this)));
		AddTest(TestListItem("IsFull()",            bind(&Tester::IsFull, this)));
        AddTest(TestListItem("Size()",              bind(&Tester::Size, this)));
        AddTest(TestListItem("Remove()",            bind(&Tester::Remove, this)));
		AddTest(TestListItem("IsElementEmpty()",    bind(&Tester::IsElementEmpty, this)));

		AddTest(TestListItem("AllocateMemory()",    bind(&Tester::AllocateMemory, this)));
		AddTest(TestListItem("DeallocateMemory()",  bind(&Tester::DeallocateMemory, this)));
		AddTest(TestListItem("Resize()",            bind(&Tester::Resize, this)));

		AddTest(TestListItem("Push()",              bind(&Tester::Push, this)));
		AddTest(TestListItem("Get()",               bind(&Tester::Get, this)));
	}

	virtual ~Tester() { }

private:
	int VectorConstructor();
	int IsEmpty();
	int IsInvalidIndex();
	int IsFull();
	int Size();
	int Remove();
	int IsElementEmpty();
	int AllocateMemory();
	int DeallocateMemory();
	int Resize();
	int Push();
	int Get();
};

int Tester::VectorConstructor()
{
    StartTestSet( "Vector Constructor", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

    {   /********************************************** TEST BEGIN */
        /** Test constructor **/
        StartTest( "Create empty Vector, check m_itemCount" );

        Vector test;

        int expectedSize = 0;
        int actualSize = test.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", expectedSize );
        Set_ActualOutput    ( "m_itemCount", actualSize );

        if ( actualSize == expectedSize )   { TestPass(); }
        else
        {
            TestFail( "Bad m_itemCount value. Make sure you're setting the size in the constructor." );
        }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        /** Test constructor **/
        StartTest( "Create empty Vector, check m_arraySize" );

        Vector test;

        int expectedSize = 0;
        int actualSize = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", expectedSize );
        Set_ActualOutput    ( "m_arraySize", actualSize );

        if ( actualSize == expectedSize )   { TestPass(); }
        else
        {
            TestFail( "Bad m_arraySize value. Make sure you're setting the size in the constructor." );
        }

        FinishTest();
    }   /********************************************** TEST END */

    {   /********************************************** TEST BEGIN */
        /** Test constructor **/
        StartTest( "Create empty Vector, check m_data" );

        Vector test;
        ostringstream oss;

        string* expected = nullptr;
        string* actual = test.m_data;

        oss << actual;
        string actualAddress = oss.str();

        Set_ExpectedOutput  ( "m_data points to", string( "nullptr" ) );
        Set_ActualOutput    ( "m_data points to", string( actualAddress ) );

        if ( actual == expected )   { TestPass(); }
        else
        {
            TestFail( "Bad m_data value. Make sure to set m_data to nullptr in the constructor." );
        }

        FinishTest();
    }   /********************************************** TEST END */

    FinishTestSet();
    return TestResult();
}

int Tester::IsEmpty()
{
    StartTestSet( "IsEmpty", { "Push()" } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if IsEmpty() has been implemented" );
        try
        {
            Vector vec;
            vec.IsEmpty();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create empty Vector. Check that IsEmpty() returns true." );

        Vector test;

        bool expectedResult = true;
        bool actualResult = test.IsEmpty();

        Set_ExpectedOutput  ( "IsEmpty()", expectedResult );
        Set_ActualOutput    ( "IsEmpty()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE Push() has been implemented" );
        Set_Comments( "Next test requires the Push() function." );
        try
        {
            Vector vec;
            vec.m_data = new string[10];
            vec.m_arraySize = 10;
            vec.Push("Hi");
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector and add item. Check that IsEmpty() returns false." );
        Set_Comments( "This test requires Push() to be implemented." );

        Vector test;
        test.Push( "Hello" );

        bool expectedResult = false;
        bool actualResult = test.IsEmpty();

        Set_ExpectedOutput  ( "IsEmpty()", expectedResult );
        Set_ActualOutput    ( "IsEmpty()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::IsInvalidIndex()
{
    StartTestSet( "IsInvalidIndex", { "AllocateMemory()" } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.m_data = new string[10];
            vec.m_arraySize = 10;
            vec.IsInvalidIndex( 0 );
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE AllocateMemory() has been implemented" );
        Set_Comments( "Next test requires the AllocateMemory() function." );
        try
        {
            Vector vec;
            vec.AllocateMemory( 10 );
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Give valid index, check that IsInvalidIndex() returns false." );

        Vector test;
        test.AllocateMemory( 10 );

        bool expectedResult = false;
        bool actualResult = test.IsInvalidIndex( 2 );

        Set_ExpectedOutput  ( "IsInvalidIndex()", expectedResult );
        Set_ActualOutput    ( "IsInvalidIndex()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Give negative index, check that IsInvalidIndex() returns true." );

        Vector test;
        test.AllocateMemory( 10 );

        bool expectedResult = true;
        bool actualResult = test.IsInvalidIndex( -5 );

        Set_ExpectedOutput  ( "IsInvalidIndex()", expectedResult );
        Set_ActualOutput    ( "IsInvalidIndex()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Give index 10, size 10 Vector, check IsInvalidIndex() returns true." );

        Vector test;
        test.AllocateMemory( 10 );

        bool expectedResult = true;
        bool actualResult = test.IsInvalidIndex( 10 );

        Set_ExpectedOutput  ( "IsInvalidIndex()", expectedResult );
        Set_ActualOutput    ( "IsInvalidIndex()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::IsFull()
{
    StartTestSet( "IsFull", { "Push()", "AllocateMemory()" } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.IsFull();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create empty array, check IsFull() returns true." );

        Vector test;

        bool expectedResult = true;
        bool actualResult = test.IsFull();

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );
        Set_Comments( "When first created, no memory is allocated and the size and item count should be 0." );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE AllocateMemory() has been implemented" );
        Set_Comments( "Next test requires the AllocateMemory() function." );
        try
        {
            Vector vec;
            vec.AllocateMemory( 10 );
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE Push() has been implemented" );
        Set_Comments( "Next test requires the Push() function." );
        try
        {
            Vector vec;
            vec.Push("Hi");
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector, add 2 items, check IsFull() returns false." );

        Vector test;
        test.AllocateMemory( 5 );
        test.Push( "one" );
        test.Push( "two" );

        bool expectedResult = false;
        bool actualResult = test.IsFull();

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create size 5 vector, add 5 items, check IsFull() returns true." );

        Vector test;
        test.AllocateMemory( 5 );
        test.Push( "one" );
        test.Push( "two" );
        test.Push( "three" );
        test.Push( "four" );
        test.Push( "five" );

        bool expectedResult = true;
        bool actualResult = test.IsFull();

        Set_ExpectedOutput  ( "IsFull()", expectedResult );
        Set_ActualOutput    ( "IsFull()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::Size()
{
    StartTestSet( "Size", { "AllocateMemory()", "Push()", "Resize()" } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.Size();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create empty Vector. Check that size is 0." );

        Vector test;

        int expectedResult = 0;
        int actualResult = test.Size();

        Set_ExpectedOutput  ( "Size()", expectedResult );
        Set_ActualOutput    ( "Size()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE AllocateMemory() has been implemented" );
        Set_Comments( "Next test requires the AllocateMemory() function." );
        try
        {
            Vector vec;
            vec.AllocateMemory( 10 );
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE Push() has been implemented" );
        Set_Comments( "Next test requires the Push() function." );
        try
        {
            Vector vec;
            vec.Push("Hi");
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector, add 5 items. Check that size is 5." );

        Vector test;
        test.AllocateMemory( 10 );
        test.Push( "one" );
        test.Push( "two" );
        test.Push( "three" );
        test.Push( "four" );
        test.Push( "five" );

        int expectedResult = 5;
        int actualResult = test.Size();

        Set_ExpectedOutput  ( "Size()", expectedResult );
        Set_ActualOutput    ( "Size()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if PREREQUISITE Resize() has been implemented" );
        Set_Comments( "Next test requires the Resize() function." );
        try
        {
            Vector vec;
            vec.AllocateMemory( 10 );
            vec.Resize();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Create Vector size 5, add 7 items. Check that size is 7." );

        Vector test;
        test.AllocateMemory( 5 );
        test.Push( "one" );
        test.Push( "two" );
        test.Push( "three" );
        test.Push( "four" );
        test.Push( "five" );
        test.Push( "six" );
        test.Push( "seven" );

        int expectedResult = 7;
        int actualResult = test.Size();

        Set_ExpectedOutput  ( "Size()", expectedResult );
        Set_ActualOutput    ( "Size()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::Remove()
{
    StartTestSet( "Remove", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.m_data = new string[5];
            vec.m_arraySize = 5;
            vec.Remove( 2 );
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that the Remove() function removes an item." );

        Vector test;
        test.m_data = new string[4];
        test.m_itemCount = 4;
        test.m_arraySize = 4;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        test.Remove( 1 );

        string expectedResult[] = { "A", "", "C", "D" };

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that the Remove() function changes the item count." );

        Vector test;
        test.m_data = new string[4];
        test.m_itemCount = 4;
        test.m_arraySize = 4;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        test.Remove( 1 );

        int expectedResult = 3;
        int actualResult = test.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", expectedResult );
        Set_ActualOutput    ( "m_itemCount", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::IsElementEmpty()
{
    StartTestSet( "IsElementEmpty", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.m_data = new string[10];
            vec.m_arraySize = 10;
            vec.IsElementEmpty( 1 );
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that IsElementEmpty() returns true for \"\" item." );

        Vector test;
        test.m_data = new string[4];
        test.m_itemCount = 4;
        test.m_arraySize = 4;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "";
        test.m_data[3] = "D";

        bool expectedResult = true;
        bool actualResult = test.IsElementEmpty( 2 );

        Set_ExpectedOutput  ( "IsElementEmpty()", expectedResult );
        Set_ActualOutput    ( "IsElementEmpty()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that IsElementEmpty() returns false for taken item." );

        Vector test;
        test.m_data = new string[4];
        test.m_itemCount = 4;
        test.m_arraySize = 4;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "";
        test.m_data[3] = "D";

        bool expectedResult = false;
        bool actualResult = test.IsElementEmpty( 1 );

        Set_ExpectedOutput  ( "IsElementEmpty()", expectedResult );
        Set_ActualOutput    ( "IsElementEmpty()", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::AllocateMemory()
{
    StartTestSet( "AllocateMemory", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.AllocateMemory( 10 );
            if ( vec.m_data != nullptr )
            {
                delete [] vec.m_data;
                vec.m_data = nullptr;
            }
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_data is pointing to an address after AllocateMemory()." );
        ostringstream oss;

        Vector test;
        test.AllocateMemory( 10 );

        string* actualResult = test.m_data;

        oss << actualResult;
        string actualAddress = oss.str();

        Set_ExpectedOutput  ( "m_data points to", string( "ANY non-null address" ) );
        Set_ActualOutput    ( "m_data points to", string( actualAddress ) );

        if ( actualResult == nullptr )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_arraySize is set after AllocateMemory()" );

        Vector test;
        test.AllocateMemory( 10 );

        int expectedResult = 10;
        int actualResult = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", expectedResult );
        Set_ActualOutput    ( "m_arraySize", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::DeallocateMemory()
{
    StartTestSet( "DeallocateMemory", {  } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.m_data = new string[5];
            vec.DeallocateMemory();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_data is pointing to nullptr after DeallocateMemory()." );
        ostringstream oss;

        Vector test;
        test.m_data = new string[10];
        test.DeallocateMemory();

        string* expectedResult = nullptr;
        string* actualResult = test.m_data;

        oss << actualResult;
        string actualAddress = oss.str();

        Set_ExpectedOutput  ( "m_data points to", string( "nullptr" ) );
        Set_ActualOutput    ( "m_data points to", string( actualAddress ) );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_arraySize is set to 0 after DeallocateMemory()" );

        Vector test;
        test.m_data = new string[10];
        test.DeallocateMemory();

        int expectedResult = 0;
        int actualResult = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", expectedResult );
        Set_ActualOutput    ( "m_arraySize", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::Resize()
{
    StartTestSet( "Resize", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.Resize();
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_data address is different after Resize()." );
        ostringstream oss;

        Vector test;
        test.m_data = new string[10];
        test.m_arraySize = 10;

        string* oldAddress = test.m_data;

        test.Resize();
        string* actualResult = test.m_data;

        oss << oldAddress;

        Set_ExpectedOutput  ( "m_data points to", string( "anything BESIDES " + oss.str() ) );

        oss << actualResult;
        Set_ActualOutput    ( "m_data points to", string( oss.str() ) );

        if ( actualResult == oldAddress )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_arraySize increases after Resize()" );

        Vector test;
        test.m_data = new string[10];
        test.m_arraySize = 10;

        int oldSize = test.m_arraySize;

        test.Resize();
        int actualResult = test.m_arraySize;

        Set_ExpectedOutput  ( "m_arraySize", string( "Anything above " + StringUtil::ToString( oldSize ) ) );
        Set_ActualOutput    ( "m_arraySize", actualResult );

        if ( actualResult == oldSize )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_itemCount stays the same after Resize()" );

        Vector test;
        test.m_data = new string[10];
        test.m_arraySize = 10;
        test.m_itemCount = 5;

        int oldSize = test.m_itemCount;

        test.Resize();
        int actualResult = test.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", oldSize );
        Set_ActualOutput    ( "m_itemCount", actualResult );

        if ( actualResult != oldSize )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that items are still in vector after Resize()" );

        Vector test;
        test.m_data = new string[4];
        test.m_arraySize = 4;
        test.m_itemCount = 4;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";
        test.Resize();

        string expectedResult[] = { "A", "B", "C", "D" };

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::Push()
{
    StartTestSet( "Push", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.m_data = new string[10];
            vec.m_arraySize = 10;
            vec.Push("Hi");
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if Push adds items to vector." );

        Vector test;
        test.m_data = new string[5];

        test.Push( "A" );
        test.Push( "B" );
        test.Push( "C" );
        test.Push( "D" );

        string expectedResult[] = { "A", "B", "C", "D" };

        bool allMatch = true;
        for ( int i = 0; i < 4; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that m_itemCount changes after Push()" );

        Vector test;
        test.m_data = new string[5];

        test.Push( "A" );
        test.Push( "B" );
        test.Push( "C" );
        test.Push( "D" );

        int expectedResult = 4;
        int actualResult = test.m_itemCount;

        Set_ExpectedOutput  ( "m_itemCount", expectedResult );
        Set_ActualOutput    ( "m_itemCount", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if Push fills in holes." );

        Vector test;
        test.m_data = new string[5];
        test.m_itemCount = 4;
        test.m_arraySize = 5;
        test.m_data[0] = "A";
        test.m_data[1] = "";
        test.m_data[2] = "B";
        test.m_data[3] = "C";
        test.m_data[4] = "D";
        test.Push( "E" );

        string expectedResult[] = { "A", "E", "B", "C", "D" };

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            Set_ExpectedOutput  ( "Element " + StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + StringUtil::ToString( i ), test.m_data[i] );

            if ( test.m_data[i] != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int Tester::Get()
{
    StartTestSet( "Get", { } );

    bool exceptOccur = false;
	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check if function has been implemented" );
        try
        {
            Vector vec;
            vec.m_data = new string[10];
            vec.m_itemCount = 10;
            vec.m_arraySize = 10;
            vec.Get( 1 );
            delete [] vec.m_data;
            vec.m_data = nullptr;
        }
        catch( runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
        }

        if ( !exceptOccur ) { TestPass(); }
	} /* TEST END **************************************************************/

	if ( exceptOccur )
	{
        FinishTestSet();
        return TestResult();
	}

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Get item at a given index." );

        Vector test;
        test.m_data = new string[5];
        test.m_itemCount = 4;
        test.m_arraySize = 5;
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        string expectedResult = "B";
        string actualResult = test.Get( 1 );

        Set_ExpectedOutput  ( "Get(1)", expectedResult );
        Set_ActualOutput    ( "Get(1)", actualResult );

        if ( actualResult != expectedResult )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that Panic() occurs for bad index (-5)" );

        Vector test;
        test.m_data = new string[5];
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        bool exceptionOccurred = false;

        try
        {
            test.Get( -5 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput  ( "Exception occurred?", "yes" );
        Set_ActualOutput    ( "Exception occurred?", (exceptionOccurred) ? "true" : "false" );

        if ( !exceptionOccurred )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

	{ /* TEST BEGIN ************************************************************/
        StartTest( "Check that Panic() occurs for bad index (> m_arraySize)" );

        Vector test;
        test.m_data = new string[5];
        test.m_data[0] = "A";
        test.m_data[1] = "B";
        test.m_data[2] = "C";
        test.m_data[3] = "D";

        bool exceptionOccurred = false;

        try
        {
            test.Get( 100 );
        }
        catch( ... )
        {
            exceptionOccurred = true;
        }

        Set_ExpectedOutput  ( "Exception occurred?", "yes" );
        Set_ActualOutput    ( "Exception occurred?", (exceptionOccurred) ? "true" : "false" );

        if ( !exceptionOccurred )
        {
            TestFail();
        }
        else
        {
            TestPass();
        }

        delete [] test.m_data;
        test.m_data = nullptr;

        FinishTest();
	} /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

#endif
