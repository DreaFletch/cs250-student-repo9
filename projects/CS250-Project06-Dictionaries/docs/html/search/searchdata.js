var indexSectionsWithContent =
{
  0: "_acdefghiklmnopqrstuvw~",
  1: "dflmst",
  2: "adglmnrst",
  3: "acdefghilmopqrstw~",
  4: "cfklmnstuv",
  5: "c",
  6: "dlq",
  7: "ot",
  8: "_el",
  9: "cp"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "related",
  8: "defines",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Friends",
  8: "Macros",
  9: "Pages"
};

