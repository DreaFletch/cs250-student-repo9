/*
DON'T MODIFY THIS FILE

If this file doesn't compile for you in Code::Blocks,
go to the Build Options (Project > Build options...)
and select the flag for
"Have g++ follow the C++11 ISO C++ language standard"
*/

#ifndef _TESTER_HPP	// Don't allow duplicates
#define _TESTER_HPP	// Don't allow duplicates

#include <iostream>
#include <string>
using namespace std;

#include "cuTEST/TesterBase.hpp"
#include "cuTEST/Menu.hpp"
#include "cuTEST/StringUtil.hpp"
#include "EXCEPTIONS/NotImplementedException.hpp"
#include "UTILITIES/Logger.hpp"

#include "DATASTRUCTURES/Dictionary.hpp"

//! A tester that runs a series of unit tests on the BinarySearchTree object, it will output a **Dictionary_result.txt** file with the results.
class Tester : public TesterBase
{
public:
    Tester()
    {
        AddTest(TestListItem("Tester::Dictionary_HashFunction()",       bind(&Tester::Dictionary_HashFunction, this)));
        AddTest(TestListItem("Tester::Dictionary_LinearProbe()",        bind(&Tester::Dictionary_LinearProbe, this)));
        AddTest(TestListItem("Tester::Dictionary_QuadraticProbe()",     bind(&Tester::Dictionary_QuadraticProbe, this)));
        AddTest(TestListItem("Tester::Dictionary_SecondHash()",         bind(&Tester::Dictionary_SecondHash, this)));
        AddTest(TestListItem("Tester::Dictionary_FindUnusedNode()",     bind(&Tester::Dictionary_FindUnusedNode, this)));
        AddTest(TestListItem("Tester::Dictionary_FindNodeWithKey()",    bind(&Tester::Dictionary_FindNodeWithKey, this)));
        AddTest(TestListItem("Tester::Dictionary_Exists()",             bind(&Tester::Dictionary_Exists, this)));
        AddTest(TestListItem("Tester::Dictionary_Insert()",             bind(&Tester::Dictionary_Insert, this)));
        AddTest(TestListItem("Tester::Dictionary_Get()",                bind(&Tester::Dictionary_Get, this)));
    }

private:
    int Dictionary_HashFunction();
    int Dictionary_LinearProbe();
    int Dictionary_QuadraticProbe();
    int Dictionary_SecondHash();
    int Dictionary_FindUnusedNode();
    int Dictionary_FindNodeWithKey();
    int Dictionary_Exists();
    int Dictionary_Insert();
    int Dictionary_Get();
};

/********************/
/* DICTIONARY TESTS */
/********************/

int Tester::Dictionary_HashFunction()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_HashFunction", 3 );

    StartTestSet( "Tester::Dictionary_HashFunction", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.HashFunction( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

//    StartTest( "Check height for empty tree" ); { /***************************** TEST BEGIN */
//        BinarySearchTree<string, string> bst;
//
//        int expectedOut = 0;
//        int actualOut = bst.GetHeight();
//
//        Set_ExpectedOutput( "height", expectedOut );
//        Set_ActualOutput  ( "height", actualOut );
//
//        if ( actualOut != expectedOut ) { TestFail(); }
//        else                            { TestPass(); }
//    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_HashFunction", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_LinearProbe()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_LinearProbe", 3 );

    StartTestSet( "Tester::Dictionary_LinearProbe", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.LinearProbe( 1, 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    Logger::OutHighlight( "First test group - Check LinearProbe value for 1 collision", "Tester::Dictionary_LinearProbe", 4 );
    for ( int i = 0; i < 4; i++ )
    {
        StartTest( "Check LinearProbe value for 1 collision" ); { /***************************** TEST BEGIN */
            Dictionary<int, string> testDict;

            int expectedOut = i+1;
            int actualOut = testDict.LinearProbe( i, 1 );

            Set_ExpectedOutput( "LinearProbe", expectedOut );
            Set_ActualOutput  ( "LinearProbe", actualOut );

            if ( actualOut != expectedOut ) { TestFail(); }
            else                            { TestPass(); }
        }  FinishTest(); /********************************************************** TEST END */
    }

    Logger::OutHighlight( "Second test group - Check LinearProbe value for 2 collisions", "Tester::Dictionary_LinearProbe", 4 );
    for ( int i = 0; i < 4; i++ )
    {
        StartTest( "Check LinearProbe value for 2 collisions" ); { /***************************** TEST BEGIN */
            Dictionary<int, string> testDict;

            int expectedOut = i+2;
            int actualOut = testDict.LinearProbe( i, 2 );

            Set_ExpectedOutput( "LinearProbe", expectedOut );
            Set_ActualOutput  ( "LinearProbe", actualOut );

            if ( actualOut != expectedOut ) { TestFail(); }
            else                            { TestPass(); }
        }  FinishTest(); /********************************************************** TEST END */
    }

    Logger::OutHighlight( "Third test group - Check LinearProbe value for 3 collisions", "Tester::Dictionary_LinearProbe", 4 );
    for ( int i = 0; i < 4; i++ )
    {
        StartTest( "Check LinearProbe value for 3 collisions" ); { /***************************** TEST BEGIN */
            Dictionary<int, string> testDict;

            int expectedOut = i+3;
            int actualOut = testDict.LinearProbe( i, 3 );

            Set_ExpectedOutput( "LinearProbe", expectedOut );
            Set_ActualOutput  ( "LinearProbe", actualOut );

            if ( actualOut != expectedOut ) { TestFail(); }
            else                            { TestPass(); }
        }  FinishTest(); /********************************************************** TEST END */
    }

    Logger::OutHighlight( "Test end", "Tester::Dictionary_LinearProbe", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_QuadraticProbe()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_QuadraticProbe", 3 );

    StartTestSet( "Tester::Dictionary_QuadraticProbe", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.QuadraticProbe( 1, 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    Logger::OutHighlight( "First test group - Check QuadraticProbe value for 1 collision", "Tester::Dictionary_QuadraticProbe", 4 );
    for ( int i = 0; i < 4; i++ )
    {
        StartTest( "Check QuadraticProbe value for 1 collision" ); { /***************************** TEST BEGIN */
            Dictionary<int, string> testDict;

            int expectedOut = i + 1;
            int actualOut = testDict.QuadraticProbe( i, 1 );

            Set_ExpectedOutput( "QuadraticProbe", expectedOut );
            Set_ActualOutput  ( "QuadraticProbe", actualOut );

            if ( actualOut != expectedOut ) { TestFail(); }
            else                            { TestPass(); }
        }  FinishTest(); /********************************************************** TEST END */
    }

    Logger::OutHighlight( "Second test group - Check QuadraticProbe value for 2 collisions", "Tester::Dictionary_QuadraticProbe", 4 );
    for ( int i = 0; i < 4; i++ )
    {
        StartTest( "Check QuadraticProbe value for 2 collisions" ); { /***************************** TEST BEGIN */
            Dictionary<int, string> testDict;

            int expectedOut = i + 4;
            int actualOut = testDict.QuadraticProbe( i, 2 );

            Set_ExpectedOutput( "QuadraticProbe", expectedOut );
            Set_ActualOutput  ( "QuadraticProbe", actualOut );

            if ( actualOut != expectedOut ) { TestFail(); }
            else                            { TestPass(); }
        }  FinishTest(); /********************************************************** TEST END */
    }

    Logger::OutHighlight( "Third test group - Check QuadraticProbe value for 3 collisions", "Tester::Dictionary_QuadraticProbe", 4 );
    for ( int i = 0; i < 4; i++ )
    {
        StartTest( "Check QuadraticProbe value for 3 collisions" ); { /***************************** TEST BEGIN */
            Dictionary<int, string> testDict;

            int expectedOut = i + 9;
            int actualOut = testDict.QuadraticProbe( i, 3 );

            Set_ExpectedOutput( "QuadraticProbe", expectedOut );
            Set_ActualOutput  ( "QuadraticProbe", actualOut );

            if ( actualOut != expectedOut ) { TestFail(); }
            else                            { TestPass(); }
        }  FinishTest(); /********************************************************** TEST END */
    }

    Logger::OutHighlight( "Test end", "Tester::Dictionary_QuadraticProbe", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_SecondHash()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_SecondHash", 3 );

    StartTestSet( "Tester::Dictionary_SecondHash", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.HashFunction2( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    Logger::OutHighlight( "First test - Check DoubleHash returns correct value", "Tester::Dictionary_SecondHash", 4 );
    StartTest( "Check DoubleHash returns correct value" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;

        int expectedOut = 6;
        int actualOut = testDict.HashFunction2( 1 );

        Set_ExpectedOutput( "HashFunction2", expectedOut );
        Set_ActualOutput  ( "HashFunction2", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */


    Logger::OutHighlight( "Second test - Check DoubleHash returns correct value", "Tester::Dictionary_SecondHash", 4 );
    StartTest( "Check DoubleHash returns correct value" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;

        int expectedOut = 5;
        int actualOut = testDict.HashFunction2( 2 );

        Set_ExpectedOutput( "HashFunction2", expectedOut );
        Set_ActualOutput  ( "HashFunction2", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_SecondHash", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_FindUnusedNode()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_FindUnusedNode", 3 );

    StartTestSet( "Tester::Dictionary_FindUnusedNode", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.FindUnusedNode( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    StartTest( "Make sure Linear Probe returns one after the previous value" ); { /***************************** TEST BEGIN */
//        BinarySearchTree<string, string> bst;
//
//        int expectedOut = 0;
//        int actualOut = bst.GetHeight();
//
//        Set_ExpectedOutput( "height", expectedOut );
//        Set_ActualOutput  ( "height", actualOut );
//
//        if ( actualOut != expectedOut ) { TestFail(); }
//        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_FindUnusedNode", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_FindNodeWithKey()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_FindNodeWithKey", 3 );

    StartTestSet( "Tester::Dictionary_FindNodeWithKey", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.FindNodeWithKey( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    StartTest( "Make sure Linear Probe returns one after the previous value" ); { /***************************** TEST BEGIN */
//        BinarySearchTree<string, string> bst;
//
//        int expectedOut = 0;
//        int actualOut = bst.GetHeight();
//
//        Set_ExpectedOutput( "height", expectedOut );
//        Set_ActualOutput  ( "height", actualOut );
//
//        if ( actualOut != expectedOut ) { TestFail(); }
//        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_FindNodeWithKey", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_Exists()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_Exists", 3 );

    StartTestSet( "Tester::Dictionary_Exists", { "Insert", "FindUnusedNode" } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.FindUnusedNode( 1 );
			testDict.Exists( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    Logger::OutHighlight( "First test - Check if Exists returns true for item that exists in dictionary", "Tester::Dictionary_Exists", 4 );
    StartTest( "Check if Exists returns true for item that exists in dictionary" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;

        testDict.Insert( 1, "hi" );

        bool expectedOut = true;
        bool actualOut = testDict.Exists( 1 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Second test - Check if Exists returns true for item that exists in dictionary", "Tester::Dictionary_Exists", 4 );
    StartTest( "Check if Exists returns true for item that exists in dictionary" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;

        Logger::OutHighlight( "Insert 'a' at 1", "Tester::Dictionary_Exists", 5 );
        testDict.Insert( 1, "a" );
        Logger::OutHighlight( "Insert 'b' at 2252", "Tester::Dictionary_Exists", 5 );
        testDict.Insert( 2252, "b" );
        Logger::OutHighlight( "Insert 'c' at 2", "Tester::Dictionary_Exists", 5 );
        testDict.Insert( 2, "c" );

        bool expectedOut = true;
        bool actualOut = testDict.Exists( 2252 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Third test - Check if Exists returns false for item NOT in dictionary", "Tester::Dictionary_Exists", 4 );
    StartTest( "Check if Exists returns false for item NOT in dictionary" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;

        bool expectedOut = 0;
        bool actualOut = testDict.Exists( 123 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_Exists", 3 );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_Insert()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_Insert" );

    StartTestSet( "Tester::Dictionary_Insert", { "Exists", "Get", "FindUnusedNode", "SetCollisionMethod", "LinearProbe", "QuadraticProbe", "HashFunction2", "HashFunction" } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.Insert( 1, "hi" );
			testDict.FindUnusedNode( 1 );
			testDict.SetCollisionMethod( LINEAR );
			testDict.LinearProbe( 1, 1 );
			testDict.QuadraticProbe( 1, 1 );
			testDict.HashFunction( 1 );
			testDict.HashFunction2( 1 );
			testDict.Exists( 1 );
			testDict.Get( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    Logger::OutHighlight( "First test - Make sure testDict inserts item into dictionary", "Tester::Dictionary_Insert", 4 );
    StartTest( "Make sure testDict inserts item into dictionary" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;
        testDict.SetCollisionMethod( LINEAR );
        testDict.Insert( 1, "A" );

        bool expectedOut = true;
        bool actualOut = testDict.Exists( 1 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Second test - Make sure testDict inserts item into dictionary with linear probe", "Tester::Dictionary_Insert", 4 );
    StartTest( "Make sure testDict inserts item into dictionary with linear probe" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;
        testDict.SetCollisionMethod( LINEAR );
        testDict.Insert( 1, "A" );
        testDict.Insert( 2252, "B" );

        bool expectedOut = true;
        bool actualOut = testDict.Exists( 2252 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Third test - Make sure testDict inserts item into dictionary with quadratic probe", "Tester::Dictionary_Insert", 4 );
    StartTest( "Make sure testDict inserts item into dictionary with quadratic probe" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;
        testDict.SetCollisionMethod( QUADRATIC );
        testDict.Insert( 1, "A" );
        testDict.Insert( 2252, "B" );
        testDict.Insert( 4503, "C" );

        bool expectedOut = true;
        bool actualOut = testDict.Exists( 4503 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Fourth test - Make sure testDict inserts item into dictionary with double hashing", "Tester::Dictionary_Insert", 4 );
    StartTest( "Make sure testDict inserts item into dictionary with double hashing" ); { /***************************** TEST BEGIN */
        Dictionary<int, string> testDict;
        testDict.SetCollisionMethod( DOUBLE );
        testDict.Insert( 1, "A" );
        testDict.Insert( 2252, "B" );
        testDict.Insert( 4503, "C" );

        bool expectedOut = true;
        bool actualOut = testDict.Exists( 4503 );

        Set_ExpectedOutput( "Exists", expectedOut );
        Set_ActualOutput  ( "Exists", actualOut );

        if ( actualOut != expectedOut ) { TestFail(); }
        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_Insert" );

    FinishTestSet();
    return TestResult();
}

int Tester::Dictionary_Get()
{
    Logger::OutHighlight( "Test begin", "Tester::Dictionary_Get", 3 );

    StartTestSet( "Tester::Dictionary_Get", {  } );
    ostringstream oss;

	/* Check if function is implemented before running other tests */
	bool exceptOccur = false;
	{
		StartTest("Check if required functions have been implemented");
		try
		{
			Dictionary<int, string> testDict;
			testDict.Get( 1 );
		}
		catch ( FunctionNotImplemented& ex ) { exceptOccur = true; }

        if (exceptOccur) { TestFail(); FinishTestSet(); return TestResult(); }
        else { TestPass(); }
	}
	/* end */

    StartTest( "Make sure Linear Probe returns one after the previous value" ); { /***************************** TEST BEGIN */
//        BinarySearchTree<string, string> bst;
//
//        int expectedOut = 0;
//        int actualOut = bst.GetHeight();
//
//        Set_ExpectedOutput( "height", expectedOut );
//        Set_ActualOutput  ( "height", actualOut );
//
//        if ( actualOut != expectedOut ) { TestFail(); }
//        else                            { TestPass(); }
    }  FinishTest(); /********************************************************** TEST END */

    Logger::OutHighlight( "Test end", "Tester::Dictionary_Get", 3 );

    FinishTestSet();
    return TestResult();
}

#endif
