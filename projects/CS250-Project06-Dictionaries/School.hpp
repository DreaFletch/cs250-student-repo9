#ifndef SCHOOL_HPP
#define SCHOOL_HPP

#include <string>
using namespace std;

struct School
{
    School() { }
    School( string name ) { this->name = name; }

    //! School's name
    string name;

    //! Output stream operator
    friend std::ostream& operator<< ( std::ostream& stream, const School& school );
};

std::ostream& operator<< ( std::ostream& stream, const School& school )
{
    stream << school.name;
    return stream;
}

#endif
