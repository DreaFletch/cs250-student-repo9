#ifndef NOT_IMPLEMENTED_EXCEPTION
#define NOT_IMPLEMENTED_EXCEPTION

#include "../UTILITIES/Logger.hpp"

#include <stdexcept>
using namespace std;

class FunctionNotImplemented : public runtime_error
{
    public:
    FunctionNotImplemented( const string& error )
        : runtime_error( error )
    {
        Logger::Out( "Exception: " + error, "FunctionNotImplemented constructor" );
    }
};


#endif
