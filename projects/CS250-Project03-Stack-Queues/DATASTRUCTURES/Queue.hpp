#ifndef QUEUE_HPP
#define QUEUE_HPP

#include "LinkedList.hpp"

#include "../UTILITIES/Logger.hpp"

template <typename T>
class Queue
{
    public:
    //! Push a new item into the back of the queue
    void Push(const T& newData );
    //! Remove the item at the front of the queue
    void Pop() noexcept;
    //! Access the data at the front of the queue
    T& Front();
    //! Get the amount of items in the queue
    int Size();
    //! Return whether the queue is empty
    bool IsEmpty();

    private:
    LinkedList<T> m_list;
};

template <typename T>
void Queue<T>::Push(const T& newData )
{
    Logger::Out( "Function Begin", "Queue::Push" );
    cout << "Queue<T>::Push - NOT IMPLEMENTED" << endl;
}

template <typename T>
void Queue<T>::Pop() noexcept
{
    Logger::Out( "Function Begin", "Queue::Pop" );
    cout << "Queue<T>::Pop - NOT IMPLEMENTED" << endl;
}

template <typename T>
T& Queue<T>::Front()
{
    Logger::Out( "Function Begin", "Queue::Front" );
    cout << "Queue<T>::Front - NOT IMPLEMENTED" << endl;
}

template <typename T>
int Queue<T>::Size()
{
    Logger::Out( "Function Begin", "Queue::Size" );

    return m_list.Size();
}

template <typename T>
bool Queue<T>::IsEmpty()
{
    Logger::Out( "Function Begin", "Queue::IsEmpty" );

    return m_list.IsEmpty();
}

#endif
