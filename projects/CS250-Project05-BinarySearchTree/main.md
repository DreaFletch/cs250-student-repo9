# Project 5: Binary Search Tree

## Viewing the documentation

Click on the **Classes** tab to view files, classes, and their documentation.
Click on the **Binary Search Tree** link to view a list of all functions.

## About

For this project, you will read the instructions from the Doxygen file
(or the comments in the code) to complete the program.

Use the **unit tests** provided to test your Binary Search Tree
as you are implementing features.

## Main menu

When you first start the program, you can choose to run the tests,
the program, or exit.

```
-------------------------------------------------------------------------------
 | MAIN MENU |
 -------------

 1.	Tests
 2.	Program
 3.	Exit

 >>
```

Just run the tests while you're working on completing the Binary Search Tree.

## Input/output files

When the program is running, it will ask what files to use for the input and output files.
Three files are provided:

```car-data-big.txt```, ```car-data-medium.txt```, and ```car-data-small.txt```

You can also create an empty file, then use the program to add new items in, one at a time.

## Data

In the tree, each node will have a key and a data value. The key is the car's
price (a lfoat), and the data is the ```CarData``` struct with its make,
model, and price.

## Suggested order...

Delete is going to be the most difficult function to implement. Other functions are
recursive. The suggested order to implement these in depends on each function's
test prerequisites, which you can view in the **test_result.html** file.

**Push** is probably the most important function to get implemented first
(that, and its recursive counterpart). That way you can test the other functions as well.

### Recursive functions

Several functions have "front-facing" public functions, and recursive private functions.

* GetInOrder
* GetPreOrder
* GetPostOrder
* GetMinKey / GetMin
* GetMaxKey / GetMax
* GetHeight
* Push / RecursivePush
* FindNode / RecursiveFindNode
* FindParentOfNode








