#ifndef _EXCEPTION_NOT_IMPLEMENTED
#define _EXCEPTION_NOT_IMPLEMENTED

#include <exception>
using namespace std;

class NotImplemented : virtual public runtime_error {
    public:
    NotImplemented(char const* const message) throw()
        : std::runtime_error(message)
    {

    }

    char const * what() const throw()
    {
        return exception::what();
    }
};

#endif
